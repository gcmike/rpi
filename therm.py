import time
import os
import RPi.GPIO as GPIO

os.system("sudo modprobe w1-gpio")
os.system("sudo modprobe w1-therm")
f = open('temp_out.txt','w')
start = time.time()
now = 0

GPIO.setmode(GPIO.BCM)
GPIO.setup(17,GPIO.OUT)
GPIO.output(17,True)

while now < start + 600:
  now = time.time()
  pth = "/sys/bus/w1/devices/"
  dr = os.listdir(pth) 
  tfile = open("/sys/bus/w1/devices/28-00000436a5b7/w1_slave")
  tfile2 = open("/sys/bus/w1/devices/28-00000440c8da/w1_slave")
  tmp = tfile.read()
  tmp2 = tfile2.read()
  templine = tmp.split("\n")[1].split(" ")[9]
  templine2 = tmp2.split("\n")[1].split(" ")[9]
  temp = float(templine[2:]) / 1000
  temp2 = float(templine2[2:]) / 1000
  f.write('time: %f and temp1: %f and temp2: %f\n'%(now, temp, temp2))
  if temp > 50 or temp2 > 50:
    break
  tfile.close()
  tfile2.close()

f.close()
GPIO.output(17,False)
